import flask 
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
import json
import urllib.parse

from security_utils import AuthenticationException, AuthorizationException, require_login, ContentException, ensure_json

class UnderscoreServiceException(Exception):
    pass

app = Flask(__name__)
try:
    if not os.environ["__DEVELOPMENT"] == None :
        app.config.from_object("config.Development")
        app.logger.warning("Starting __events app in DEVELOPMENT mode. This has some security implications, so if this is unexpected, QUIT NOW.")
except:
    app.config.from_object("config.Production")
    app.logger.info("Starting __events app in normal mode.")

if app.config["PUBLIC_KEY"] == b"" or app.config["SQLALCHEMY_DATABASE_URI"] == "" or app.config["AUTH_SERVICE_URL"] == "": 
    app.logger.error("The ENV is not setup correctly. Quitting.")
    quit(-1)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

from models import EventType, EventTypeTranslator, Event, EventTranslator, EventShare, EventShareTranslator

@app.route("/")
def index():
    try:
        require_login(required_permissions=["__events"])
    except AuthenticationException:
        return flask.redirect(app.config["AUTH_SERVICE_URL"] + "/login?next=" + urllib.parse.quote(flask.request.url))
    except AuthorizationException:
        return flask.redirect("/#!/error/permissions")

    return flask.render_template("main.html") 


@app.route("/api/types", methods=["GET", "POST"])
def api_types():
    try:
        require_login(required_permissions=["__events"])
    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access the event types list." % (flask.g.user["email"]))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    if flask.request.method == "GET":
        d = {}
        d["error_msgs"] = []
        errors = False

        # Get a full listing of event types owned by this user
        types_list = []
        try:
            types_objs = db.session.query(EventType).filter_by(owner_id = flask.g.user["id"])
            type_translator = EventTypeTranslator()

            for type_obj in types_objs:
                types_list.append(type_translator.to_dict(type_obj))

            # Next, we need to get a list of all event types that are shared with this user, and add them to the list as well (if they aren't already there)
            share_objs = db.session.query(EventShare).filter_by(user_id = flask.g.user["id"])

            for share_obj in share_objs:
                type_obj = db.session.query(EventType).filter_by(id = share_obj.type_id).first()
                type_in_list = False

                for type_dict in types_list:
                    if type_obj.id == type_dict["id"]:
                        type_in_list = True

                if not type_in_list:
                    types_list.append(type_translator.to_dict(type_obj))

            resp = flask.Response(json.dumps(types_list), status = 200)
            resp.mimetype = "application/json"
            return resp
        except:
            d["error_msgs"].append("Failed to retrieve event types.")
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp
    # Create a new event type
    if flask.request.method == "POST":
        d = {}
        d["error_msgs"] = []
        errors = False

        try:
            ensure_json(flask.request)
        except ContentException:
            d = {}
            d["error_msgs"] = ["Incorrect content type."]
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        type_translator = EventTypeTranslator()

        new_type_dict = flask.request.get_json()
        # Users should not be setting owner ID manually, this overrides anything they put.
        new_type_dict["owner_id"] = flask.g.user["id"]
        type_obj, error_msgs = type_translator.new_from_dict(new_type_dict)

        if len(error_msgs):
            d["error_msgs"] = d["error_msgs"] + error_msgs
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        try:
            db.session.add(type_obj)
            db.session.commit()
            return api_type(type_obj.id)

        except:
            d["error_msgs"].append("Failed to create new event type. Database session did not accept object.")
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

# REST interface to the given event type
@app.route("/api/types/<int:id>", methods=["GET", "PUT", "DELETE"])
def api_type(id):
    try:
        require_login(required_permissions=["__events"])
    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access event type ID '%d'." % (flask.g.user["email"], id))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    # Get object referred to by ID
    type_obj = None
    user_owns_type = False

    # Try to see if this is a shared type first.
    share_obj = db.session.query(EventShare).filter_by(type_id = id).filter_by(user_id = flask.g.user["id"]).first()
    if not share_obj == None:
        type_obj = db.session.query(EventType).filter_by(id = id).first()
        user_owns_type = False
    else:
        # There is no share with this event type, we must own it. 
        type_obj = db.session.query(EventType).filter_by(owner_id = flask.g.user["id"]).filter_by(id = id).first()
        user_owns_type = True

    if type_obj == None:
        # Object was not found.
        d = {}
        d["error_msgs"] = []
        d["error_msgs"].append("Object not found.")
        resp = flask.Response(json.dumps(d), status = 404)
        resp.mimetype = "application/json"
        return resp

    type_translator = EventTypeTranslator()

    if flask.request.method == "PUT":
        d = {}
        d["error_msgs"] = []
        errors = False

        if not user_owns_type:
            d["error_msgs"].append("You can't edit a shared event configuration.")
            resp = flask.Response(json.dumps(d), status = 403)
            resp.mimetype = "application/json"
            return resp

        # Get the user's update dictionary
        update_type_dict = flask.request.get_json()

        type_obj, error_msgs = type_translator.update_from_dict(type_obj, update_type_dict)

        if len(error_msgs) > 0:
            d["error_msgs"] = d["error_msgs"] + error_msgs
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        try:
            db.session.commit()

            resp = flask.Response(json.dumps(type_translator.to_dict(type_obj)), status = 200)
            resp.mimetype = "application/json"
            return resp

        except:
            d["error_msgs"].append("Failed to update eventtype. The database rejected the update.")
            raise UnderscoreServiceException


    elif flask.request.method == "DELETE":
        d = {}
        d["error_msgs"] = []

        if not user_owns_type:
            d["error_msgs"].append("You can't delete a shared event configuration.")
            resp = flask.Response(json.dumps(d), status = 403)
            resp.mimetype = "application/json"
            return resp

        try:
            tmp_type_name = type_obj.name
            db.session.delete(type_obj)
            db.session.commit()

            # Success
            d["success_msgs"] = []
            d["success_msgs"].append("Event type '%s' was deleted." % tmp_type_name)
            resp = flask.Response(json.dumps(d), status = 200)
            resp.mimetype = "application/json"
            return resp

        except UnderscoreServiceException:
            d["error_msgs"].append("Failed to delete event type. The database rejected the deletion.")
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp

    # GET or None calls.
    
    resp = flask.Response(json.dumps(type_translator.to_dict(type_obj)), status = 200)
    resp.mimetype = "application/json"
    return resp

@app.route("/api/types/<int:type_id>/events", methods=["GET", "POST"])
def api_events(type_id):
    try:
        require_login(required_permissions=["__events"])
    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access the event types list." % (flask.g.user["email"]))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    # Get object referred to by ID
    type_obj = None
    user_owns_type = False

    # Try to see if this is a shared type first.
    share_obj = db.session.query(EventShare).filter_by(type_id = type_id).filter_by(user_id = flask.g.user["id"]).first()
    if not share_obj == None:
        type_obj = db.session.query(EventType).filter_by(id = type_id).first()
        user_owns_type = False
    else:
        # There is no share with this event type, we must own it. 
        type_obj = db.session.query(EventType).filter_by(owner_id = flask.g.user["id"]).filter_by(id = type_id).first()
        user_owns_type = True

    if type_obj == None:
        d = {}
        d["error_msgs"] = ["Event type not found."]
        resp = flask.Response(json.dumps(d), status = 404)
        resp.mimetype = "application/json"
        return resp

    if flask.request.method == "GET":
        d = {}
        d["error_msgs"] = []
        errors = False

        # Get a full listing of events for this event type
        events_list = []
        try:
            events_objs = db.session.query(Event).filter_by(type = type_id)
            event_translator = EventTranslator(db)

            for event_obj in events_objs:
                events_list.append(event_translator.to_dict(event_obj))

            resp = flask.Response(json.dumps(events_list), status = 200)
            resp.mimetype = "application/json"
            return resp
        except:
            d["error_msgs"].append("Failed to retrieve events.")
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp
    # Create a new event
    if flask.request.method == "POST":
        d = {}
        d["error_msgs"] = []
        errors = False

        try:
            ensure_json(flask.request)
        except ContentException:
            d = {}
            d["error_msgs"] = ["Incorrect content type."]
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        if not user_owns_type and not share_obj.can_write:
            d["error_msgs"] = ["You don't have permission to create that."]
            resp = flask.Response(json.dumps(d), status = 404)
            resp.mimetype = "application/json"
            return resp

        event_translator = EventTranslator(db)

        new_event_dict = flask.request.get_json()
        if new_event_dict == None:
            new_event_dict = {}

        new_event_dict["type"] = type_id

        event_obj, error_msgs = event_translator.new_from_dict(new_event_dict)

        if len(error_msgs):
            d["error_msgs"] = d["error_msgs"] + error_msgs
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        try:          
            db.session.add(event_obj)
            db.session.commit()
            return api_event(event_obj.id)

        except:
            d["error_msgs"].append("Failed to create new event type. Database session did not accept object.")
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

# REST interface to the given events
@app.route("/api/events/<int:event_id>", methods=["GET", "DELETE"])
def api_event(event_id):
    try:
        require_login(required_permissions=["__events"])
    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access event type ID '%d'." % (flask.g.user["email"], id))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    # Get object referred to by ID
    event_obj = db.session.query(Event).filter_by(id = event_id).first()

    if event_obj == None:
        # Object was not found.
        d = {}
        d["error_msgs"] = []
        d["error_msgs"].append("Object not found.")
        resp = flask.Response(json.dumps(d), status = 404)
        resp.mimetype = "application/json"
        return resp

    # Get object referred to by ID
    type_obj = None
    user_owns_type = False

    # Try to see if this is a shared type first.
    share_obj = db.session.query(EventShare).filter_by(type_id = event_obj.type).filter_by(user_id = flask.g.user["id"]).first()
    if not share_obj == None:
        type_obj = db.session.query(EventType).filter_by(id = event_obj.type).first()
        user_owns_type = False
    else:
        # There is no share with this event type, we must own it. 
        type_obj = db.session.query(EventType).filter_by(owner_id = flask.g.user["id"]).filter_by(id = event_obj.type).first()
        user_owns_type = True

    if type_obj == None:
        d = {}
        d["error_msgs"] = ["Object not found."]
        resp = flask.Response(json.dumps(d), status = 404)
        resp.mimetype = "application/json"
        return resp

    event_translator = EventTranslator(db)

    if flask.request.method == "DELETE":
        d = {}
        d["error_msgs"] = []

        if not user_owns_type and not share_obj.can_write:
            d["error_msgs"] = ["You don't have permission to delete that."]
            resp = flask.Response(json.dumps(d), status = 404)
            resp.mimetype = "application/json"
            return resp

        try:
            tmp_event_name = event_obj.time
            db.session.delete(event_obj)
            db.session.commit()

            # Success
            d["success_msgs"] = []
            d["success_msgs"].append("Event '%s' was deleted." % tmp_event_name)
            resp = flask.Response(json.dumps(d), status = 200)
            resp.mimetype = "application/json"
            return resp

        except:
            d["error_msgs"].append("Failed to delete event. The database rejected the deletion.")
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp

    # GET or None calls.
    resp = flask.Response(json.dumps(event_translator.to_dict(event_obj)), status = 200)
    resp.mimetype = "application/json"
    return resp

@app.route("/api/types/<int:type_id>/shares", methods=["GET", "POST"])
def api_shares(type_id):
    try:
        require_login(required_permissions=["__events"])
    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access the event shares list." % (flask.g.user["email"]))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    # Check to ensure this event type is actually owned by this user
    type_obj = db.session.query(EventType).filter_by(owner_id = flask.g.user["id"]).filter_by(id = type_id).first()

    if type_obj == None:
        d = {}
        d["error_msgs"] = ["Event type not found."]
        resp = flask.Response(json.dumps(d), status = 404)
        resp.mimetype = "application/json"
        return resp

    if flask.request.method == "GET":
        d = {}
        d["error_msgs"] = []
        errors = False

        # Get a full listing of shares for this event type
        shares_list = []
        try:
            shares_objs = db.session.query(EventShare).filter_by(type_id = type_id)
            share_translator = EventShareTranslator(db)

            for shares_obj in shares_objs:
                shares_list.append(share_translator.to_dict(shares_obj))

            resp = flask.Response(json.dumps(shares_list), status = 200)
            resp.mimetype = "application/json"
            return resp
        except:
            d["error_msgs"].append("Failed to retrieve events shares.")
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp

    # Create a new event share
    if flask.request.method == "POST":
        d = {}
        d["error_msgs"] = []
        errors = False

        try:
            ensure_json(flask.request)
        except ContentException:
            d = {}
            d["error_msgs"] = ["Incorrect content type."]
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        share_translator = EventShareTranslator(db)

        new_share_dict = flask.request.get_json()
        if new_share_dict == None:
            new_share_dict = {}

        new_share_dict["type_id"] = type_id

        share_obj, error_msgs = share_translator.new_from_dict(new_share_dict)

        if len(error_msgs):
            d["error_msgs"] = d["error_msgs"] + error_msgs
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        try:          
            db.session.add(share_obj)
            db.session.commit()
            return api_share(share_obj.id)

        except:
            d["error_msgs"].append("Failed to create new event share. Database session did not accept object.")
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

# REST interface to the given events
@app.route("/api/share/<int:share_id>", methods=["GET", "PUT", "DELETE"])
def api_share(share_id):
    try:
        require_login(required_permissions=["__events"])
    except AuthenticationException:
        d = {}
        d["error_msgs"] = ["You must be authenticated."]
        resp = flask.Response(json.dumps(d), status = 401)
        resp.mimetype = "application/json"
        return resp
    except AuthorizationException:
        d = {}
        d["error_msgs"] = ["You do not have permission to access that."]
        app.logger.warning("AuthorizationException: User '%s' tried to access event share ID '%d'." % (flask.g.user["email"], id))
        resp = flask.Response(json.dumps(d), status = 403)
        resp.mimetype = "application/json"
        return resp

    # Get object referred to by ID
    share_obj = db.session.query(EventShare).filter_by(id = share_id).first()

    if share_obj == None:
        # Object was not found.
        d = {}
        d["error_msgs"] = []
        d["error_msgs"].append("Object not found.")
        resp = flask.Response(json.dumps(d), status = 404)
        resp.mimetype = "application/json"
        return resp

    # Check to ensure this event type is actually owned by this user
    type_obj = db.session.query(EventType).filter_by(owner_id = flask.g.user["id"]).filter_by(id = share_obj.type_id).first()

    if type_obj == None:
        d = {}
        d["error_msgs"] = ["Object not found."]
        resp = flask.Response(json.dumps(d), status = 404)
        resp.mimetype = "application/json"
        return resp

    share_translator = EventShareTranslator(db)

    if flask.request.method == "PUT":
        d = {}
        d["error_msgs"] = []
        errors = False

        # Get the user's update dictionary
        update_share_dict = flask.request.get_json()

        share_obj, error_msgs = share_translator.update_from_dict(share_obj, update_share_dict)

        if len(error_msgs) > 0:
            d["error_msgs"] = d["error_msgs"] + error_msgs
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp

        try:
            db.session.commit()

            resp = flask.Response(json.dumps(share_translator.to_dict(share_obj)), status = 200)
            resp.mimetype = "application/json"
            return resp

        except:
            d["error_msgs"].append("Failed to update share. The database rejected the update.")
            resp = flask.Response(json.dumps(d), status = 400)
            resp.mimetype = "application/json"
            return resp


    elif flask.request.method == "DELETE":
        d = {}
        d["error_msgs"] = []
        try:
            db.session.delete(share_obj)
            db.session.commit()

            # Success
            d["success_msgs"] = []
            d["success_msgs"].append("Share was deleted.")
            resp = flask.Response(json.dumps(d), status = 200)
            resp.mimetype = "application/json"
            return resp

        except:
            d["error_msgs"].append("Failed to delete share. The database rejected the deletion.")
            resp = flask.Response(json.dumps(d), status = 500)
            resp.mimetype = "application/json"
            return resp

    # GET or None calls.
    resp = flask.Response(json.dumps(share_translator.to_dict(share_obj)), status = 200)
    resp.mimetype = "application/json"
    return resp

# Error Handlers    
@app.errorhandler(404)
def error_not_found_handler(e):
    return flask.redirect("/#!/error/not-found")

# In theory, this only gets thrown on API calls.
@app.errorhandler(500)
def error_not_found_handler(e):
    d = {}
    d["error_msgs"] = ["Unexpected server error. Something went wrong."]
    app.logger.error(e)
    resp = flask.Response(json.dumps(d), status = 500)
    resp.mimetype = "application/json"
    return resp


@app.route("/api/config")
def api_config():
    try:
        require_login(required_permissions=["__events"])
    except AuthenticationException:
        return flask.redirect(app.config["AUTH_SERVICE_URL"] + "/login?next=" + urllib.parse.quote(flask.request.url))
    except AuthorizationException:
        return flask.redirect("/#!/error/permissions")

    d = {}
    d["users_host"] = app.config["AUTH_SERVICE_URL"]
    resp = flask.Response(json.dumps(d), status = 200)
    resp.mimetype = "application/json"
    return resp


if __name__ == "__main__":
    app.run()