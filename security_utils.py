import jwt
import datetime
import functools
import flask

class AuthenticationException(Exception):
    pass

class AuthorizationException(Exception):
    pass

def require_login(requires_admin = False, required_permissions = []):
    token_properties = {}
    try:
        token = flask.request.cookies["__token"]

        try:
            token_properties = jwt.decode(token, flask.current_app.config["PUBLIC_KEY"], algorithms="RS256")
        except:
            # This check exists to allow developers to not have to standup an entire __users service just for testing purposes. Basically, you can use any JWT in the __token field, and even if it's expired or invalid, it'll be allowed. There are some obvious security implications here, but basically, if your config is being edited, you're already screwed anyhow.
            if flask.current_app.config["FLASK_ENV"] == "development" and flask.current_app.config["DEVELOPMENT"] and flask.current_app.config["DEBUG"]:
                flask.current_app.logger.warning("Bypassed user authorization check in require_user_login(). This means that a hacker can access ALL data in your __services. This should only be used for development.")
                token_properties = jwt.decode(token, "", algorithms= "RS256", verify=False)
            else:
                raise AuthenticationException

        # Ensure the user object actually exists
        if not token_properties["user"]:
            raise AuthenticationException

        # From here on we know the token is valid, but whether they have permission to access is another story.

        # Note to self and others: I'm about 99% sure the context of the flask.g object is constrained to the single request it's currently handling (per http://flask.pocoo.org/docs/1.0/appcontext/#storing-data), but just in case I'm wrong, here's the attack path. Basically it's a TOCTOU vuln that exists when two users are checking thier login at the same time, causing one user's permission to be used in another user's request handling. This would be bad.
        flask.g.user = token_properties["user"]

        # Admins check
        if requires_admin and not token_properties["user"]["is_admin"]:
            raise AuthorizationException

        # Permissions check, admins automatically pass any check.
        if len(required_permissions) > 0 and not token_properties["user"]["is_admin"]:

            # Build of checklist of all permissions that must be met. Any one of these failing will result in an AuthZ exception
            permissions_checklist = {}
            for required_privilege in required_permissions:
                required_privilege = required_privilege.strip()
                permissions_checklist[required_privilege] = False

            for user_permission in token_properties["user"]["permissions"].split(","):
                user_permission = user_permission.strip()
                try:
                    permissions_checklist[user_permission] = True
                except KeyError:
                    pass

            for item in permissions_checklist.keys():
                if permissions_checklist[item] == False:
                    raise AuthorizationException

    except KeyError:
        raise AuthenticationException
    
class ContentException(Exception):
    pass

def ensure_json(request):
    try:
        # this split is because sometimes the content type can have a charset option after it.
        ct = request.content_type.split(";")[0]
        if not ct == "application/json":
            raise ContentException
    except:
        raise ContentException