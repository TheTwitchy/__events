# \_\_events
Underscore service event time tracking. Used to track when certain events happen, as well as some basic data per event.


## Screenshots
See the [wiki](https://gitlab.com/TheTwitchy/__events/-/wikis/home).

## Requirements
* A dokku instance (that is probably already hosting [\_\_users](https://gitlab.com/TheTwitchy/__users))
* A running [\_\_users](https://gitlab.com/TheTwitchy/__users) service.

## Installation
### Production
We recommend you use dokku (a self-hosted Heroku, link is https://github.com/dokku/dokku) for automated deployment. Manual deployment without dokku is possible, but more painful. You could also just use Heroku, I believe it would require zero code changes, but we have a thing about relying on proprietary services that could disappear at any time.

#### Automated Deployment 
The following example uses the domain `myservices.com` for all deployment, where \_\_events is accessed via `events.myservices.com`, and other services are accessed via `someotherservice.myservices.com`. Replace with your own DNS name as needed.

* From local git repo.
    * `DOKKU_HOST=myservices.com dokku_client.sh apps:create events`
        * Sets up the local repo so that you can deploy to your Dokku host. Will not be needed on updates.
    * `dokku_client.sh config:set AUTH_SERVICE_URL=https://users.myservices.com PUBLIC_KEY_B64=$(cat myservices.key.pub|base64 -w0)`
        * This instructs the dokku host to set ENV variables for the running application. Specifically, you will need the public key you generated when you stood up the \_\_ service. Will not be needed on updates.
    * `dokku_client.sh postgres:create eventsdb`
        * Creates a database specifically for the app. Will not be needed on updates.
    * `dokku_client.sh postgres:link eventsdb events`
        * Links the created database to the start app. This also automatically sets the DATABASE_URL ENV variable needed by the application. Will not be needed on updates.
    * `git push dokku master`
        * This deploys the \_\_service to the dokku host.
    * `dokku_client.sh run python manage.py db upgrade`
        * Migrates the database tables and columns to what is required by the app. This may be needed on updates where the models change, but running it when not needed will not cause issues.


#### Maintenence
For when you need to figure out what's wrong. The full dokku docs should be helpful, but the following are a few specifically helpful commands.

* `dokku apps:list`
    * Shows you a list of all apps currently running.
* `dokku_client.sh logs`
    * This will show you the current logs of all apps. It should give you an idea of what's wrong with your installation, or what problem the app is running into.

#### HTTPS Setup
Optional, but recommended. This forces you to access your \_\_services over HTTPS instead of HTTP, thereby securing the connection between you and your server whenever you use \_\_services. You can read more about the process at http://dokku.viewdocs.io/dokku/configuration/ssl/.

* On dokku server (unfortunately, no way to do this remotely yet):
    * Install certbot for LetsEncrypt. There are plenty of guides about how to do this. You should only need to do this once.
    * `certbot certonly --manual --preferred-challenges dns --email youremail@gmail.com --agree-tos -d *.myservices.com`
        * This is a manual process to generate a wildcard certificate for the domain `myservices.com`. A wildcard certificate is valid for any form of `appname.myservices.com`, so instead of generating one certificate for every app (which you could do if so inclined), you have one certifcate that all apps will share. If you have a DNS provider that supports automatic renewal, then change the command to use that, otherwise you will have to do this every 90 days to renew the certificate.
    * `cd /etc/letsencrypt/live/myservices.com/ && cp fullchain.pem server.crt && cp privkey.pem server.key && tar cvf dokku-cert-bundle.tar`
        * Dokku excepts a certificate bundle to contain one .key file and one .crt file, so we're just converting them a bit, then bundling them using `tar`. Do this whenever certificates get regenerated.
    * `cd /etc/letsencrypt/live/myservices.com/ && dokku certs:add events < dokku-cert-bundle.tar`
        * This instructs the dokku instance to use this cert bundle with this \_\_service. Do this whenever certificates get regenerated.

### Development
#### VirtualENV
* `pip install virtualenv`
* `virtualenv venv`

#### Install Dependencies
* `source venv/bin/activate`
* `pip install -r requirements.txt`

#### Setup Postgresql
* `sudo apt install postgresql`
* `sudo su postgres`
* `psql`
    * `CREATE DATABASE __events_dev;` 
    * `CREATE USER __user_dev WITH ENCRYPTED PASSWORD 'enter_password_here';`
    * `GRANT ALL PRIVILEGES ON DATABASE __events_dev TO __user_dev;`
    * `\q`
* `python manage.py db init`

##### Updates Models in Database
* `python manage.py db migrate`
    * Only needs to be run on updates to model.py.
* `python manage.py db upgrade`
    * Run this to update the layout of your DB when first start development and whenever model changes are made.

#### Run Development HTTP Server
* `source dev_env && python manage.py runserver`
* Then visit [the development server](http://localhost1.dev.thetwitchy.com:5000/). For an explanation of why this URL is used, see config.py in the \_\_users repo.