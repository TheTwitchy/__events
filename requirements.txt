# Standard libraries
alembic
cryptography
flask
flask-migrate
flask-script
flask-sqlalchemy
gunicorn
psycopg2-binary
pyjwt
sqlalchemy

# __events app specific libraries