from app import db
import datetime 
import flask

class EventTypeTranslator():

    def to_dict(self, event_type_obj):
        eventtype = {}
        eventtype["id"] = event_type_obj.id
        eventtype["owner_id"] = event_type_obj.owner_id
        eventtype["name"] = event_type_obj.name
        eventtype["description"] = event_type_obj.description
        eventtype["has_data"] = event_type_obj.has_data
        eventtype["data_unit"] = event_type_obj.data_unit
        eventtype["priority"] = event_type_obj.priority

        return eventtype

    def validate_dict(self, event_type_dict, is_new):
        error_msgs = []

        # Check name
        try:
            tmp = event_type_dict["name"]
            # Check to ensure the correct type
            if not isinstance(tmp, str):
                error_msgs.append("Name must be a string")
            # Check the string length 
            if tmp == "":
                error_msgs.append("Name is empty")

            # Check the string length 
            if len(tmp) > 256:
                error_msgs.append("Name is too long")
        except KeyError:
            # Check to ensure this is provided
            if is_new:
                error_msgs.append("An event type name is required")

        # Check description
        try:
            tmp = event_type_dict["description"]
            if not tmp == None:
                # Check to ensure the correct type
                if not isinstance(tmp, str):
                    error_msgs.append("Description must be a string")
                # Check the string length 
                if len(tmp) > 256:
                    error_msgs.append("Description is too long")
        except KeyError:
            pass

        # Check has_data
        try:
            tmp = event_type_dict["has_data"]
            # Check to ensure the correct type
            if not (tmp == True or tmp == False):
                error_msgs.append("The 'has_data' field is an incorrect type")
        except KeyError:
            # Check to ensure this is provided 
            if is_new:
                error_msgs.append("You must specify whether this event type tracks data via the 'has_data' field")

        # Check data_unit 
        try:
            tmp = event_type_dict["data_unit"]

            if not tmp == None:
                # Check to ensure the correct type
                if not isinstance(tmp, str):
                    error_msgs.append("Data type must be a string")
                # Check the string length 
                if len(tmp) > 256:
                    error_msgs.append("Data type is too long")
        except KeyError:
            # Check to ensure this is provided
            if is_new and event_type_dict["has_data"]:
                error_msgs.append("A data type for tracked data must be specified")

        # Check priority
        try:
            tmp = event_type_dict["priority"]
            # Check to ensure the correct type
            if not isinstance(tmp, int):
                error_msgs.append("Priority must be an integer")
        except KeyError:
            # Check to ensure this is provided
            if is_new:
                error_msgs.append("An event type priority is required")

        return error_msgs

    def new_from_dict(self, event_type_dict):
        error_msgs = self.validate_dict(event_type_dict, is_new = True)

        if (len(error_msgs) > 0):
            # If there are any errors after validation, we want to stop immediately
            return None, error_msgs

        # From this point onwards, input data is expected to be acceptable
        new_obj = EventType(flask.g.user["id"], event_type_dict["name"], has_data = event_type_dict["has_data"], priority = event_type_dict["priority"])

        try:
            new_obj.description = event_type_dict["description"]
        except KeyError:
            pass

        try:
            new_obj.data_unit = event_type_dict["data_unit"]
        except KeyError:
            pass

        return new_obj, error_msgs

    def update_from_dict(self, event_type_obj, event_type_dict):
        error_msgs = self.validate_dict(event_type_dict, is_new = False)

        if (len(error_msgs) > 0):
            # If there are any errors after validation, we want to stop immediately
            return None, error_msgs

        # From this point onwards, input data is expected to be acceptable
        try:
            event_type_obj.name = event_type_dict["name"]
        except KeyError:
            pass

        try:
            event_type_obj.description = event_type_dict["description"]
        except KeyError:
            pass

        try:
            event_type_obj.has_data = event_type_dict["has_data"]
        except KeyError:
            pass

        try:
            event_type_obj.data_unit = event_type_dict["data_unit"]
        except KeyError:
            pass

        try:
            event_type_obj.priority = event_type_dict["priority"]
        except KeyError:
            pass

        return event_type_obj, error_msgs


class EventType(db.Model):
    __tablename__ = "eventtypes"

    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, nullable = False)
    name = db.Column(db.String(256), nullable = False)
    description = db.Column(db.String(1024), nullable = True)
    has_data = db.Column(db.Boolean, default = False, nullable = False)
    data_unit = db.Column(db.String(256), nullable = True)
    priority = db.Column(db.Integer, nullable = False, default = 0)

    __table_args__ = (db.UniqueConstraint("owner_id", "name"),)
    
    def __init__(self, owner_id, name, description = None, has_data = False, data_unit = None, priority = 0):
        self.owner_id = owner_id
        self.name = name
        self.description = description
        self.has_data = has_data
        self.data_unit = data_unit
        self.priority = priority

    def __str__(self):
        return "EventType: id=" + str(self.id) + ", owner_id=" + str(self.owner_id) + ", name=" + self.name + ", has_data=" + str(self.has_data) + ", priority=" + str(self.priority) 


class EventTranslator():

    def __init__(self, db):
        self.db = db

    def to_dict(self, event_obj):
        dictionary = {}
        dictionary["id"] = event_obj.id
        dictionary["type"] = event_obj.type
        dictionary["time"] = event_obj.time.strftime("%Y-%m-%dT%H:%M:%SZ")
        dictionary["data"] = event_obj.data

        return dictionary

    def validate_dict(self, event_dict, is_new):
        error_msgs = []

        event_type_obj = None

        # Check type ID
        try:
            tmp = event_dict["type"]
            # Check to ensure the correct type
            if not isinstance(tmp, int):
                error_msgs.append("Event type must refer to an existing type ID")

            # Get object referred to by ID
            type_obj = None
            user_owns_type = False

            # Try to see if this is a shared type first
            share_obj = db.session.query(EventShare).filter_by(type_id = tmp).filter_by(user_id = flask.g.user["id"]).first()
            if not share_obj == None:
                event_type_obj = db.session.query(EventType).filter_by(id = tmp).first()
                user_owns_type = False
            else:
                # There is no share with this event type, we must own it. 
                event_type_obj = db.session.query(EventType).filter_by(owner_id = flask.g.user["id"]).filter_by(id = tmp).first()
                user_owns_type = True
            
            if event_type_obj == None:
                error_msgs.append("Event type ID not found")

        except KeyError:
            # Check to ensure this is provided
            if is_new:
                error_msgs.append("An event type ID is required")

        # Check data
        if not event_type_obj == None and event_type_obj.has_data:
            try:
                tmp = event_dict["data"]
                # Check to ensure the correct type
                if not isinstance(tmp, int):
                    error_msgs.append("Data must be an integer")
            except KeyError:
                # Check to ensure this is provided
                if is_new:
                    error_msgs.append("Event data is required")

        return error_msgs

    def new_from_dict(self, event_dict):
        error_msgs = self.validate_dict(event_dict, is_new = True)

        if (len(error_msgs) > 0):
            # If there are any errors after validation, we want to stop immediately
            return None, error_msgs

        # From this point onwards, input data is expected to be acceptable
        new_obj = Event(event_dict["type"])

        try:
            new_obj.data = event_dict["data"]
        except KeyError:
            pass

        return new_obj, error_msgs


class Event(db.Model):
    __tablename__ = "event"

    id = db.Column(db.Integer, primary_key = True)
    # This points to the ID of the EventType this corresponds to. This could be a foreign key, but I'd rather manage this manually.
    type = db.Column(db.Integer, nullable = False)
    time = db.Column(db.DateTime(timezone = True), nullable = False, default = datetime.datetime.utcnow)
    data = db.Column(db.Integer, nullable = True)

    def __init__(self, type, data = ""):
        self.type = type
        if not data == "":
            self.data = data

    def to_dict(self):
        event = {}
        event["id"] = self.id
        event["type"] = self.type
        event["time"] = self.time
        
        if not self.data == None:
            event["data"] = self.data

        return event


class EventShareTranslator():

    def __init__(self, db):
        self.db = db

    def to_dict(self, obj):
        dictionary = {}
        dictionary["id"] = obj.id
        dictionary["type_id"] = obj.type_id
        dictionary["user_id"] = obj.user_id
        dictionary["can_write"] = obj.can_write

        return dictionary

    def validate_dict(self, event_share_dict):
        error_msgs = []

        event_type_obj = None
        user_obj = None

        # Check type ID
        try:
            tmp = event_share_dict["type_id"]
            # Check to ensure the correct type
            if not isinstance(tmp, int):
                error_msgs.append("Event type must refer to an existing type ID")

            # This ensures that we can only make EventShare Objects for a type we own.
            event_type_obj = self.db.session.query(EventType).filter_by(owner_id = flask.g.user["id"]).filter_by(id = tmp).first()
            
            if event_type_obj == None:
                error_msgs.append("Event type ID not found")

        except KeyError:
            # Check to ensure this is provided
            error_msgs.append("An event type is required")

        # Check that user ID is sane (although not necesarily valid)
        try:
            tmp = event_share_dict["user_id"]
            # Check to ensure the correct type
            if not isinstance(tmp, int):
                error_msgs.append("User field must refer to an existing user ID")

        except KeyError:
            # Check to ensure this is provided
            error_msgs.append("A user is required")

        # Check can_write
        try:
            tmp = event_share_dict["can_write"]
            # Check to ensure the correct type
            if not (tmp == True or tmp == False):
                error_msgs.append("The 'can_write' field is an incorrect type")
        except KeyError:
            # Check to ensure this is provided 
            error_msgs.append("You must specify whether the user can write new events to this event type")

        return error_msgs

    def new_from_dict(self, event_share_dict):
        error_msgs = self.validate_dict(event_share_dict)

        if (len(error_msgs) > 0):
            # If there are any errors after validation, we want to stop immediately
            return None, error_msgs

        # From this point onwards, input data is expected to be acceptable
        new_obj = EventShare(event_share_dict["type_id"], event_share_dict["user_id"], event_share_dict["can_write"])

        return new_obj, []


# This object maps an EventType to a user, which means that that the user can at a minimum read this event type and all it's associated events. The 'can_write' flag specifies if the user can also generate new events. Only the owner can change the config of the event itself.
class EventShare(db.Model):
    __tablename__ = "eventshare"

    id = db.Column(db.Integer, primary_key = True)
    # This points to the ID of the EventType this corresponds to. This could be a foreign key, but I'd rather manage this manually.
    type_id = db.Column(db.Integer, nullable = False)
    user_id = db.Column(db.Integer, nullable = False)
    can_write = db.Column(db.Boolean, nullable = False)

    __table_args__ = (db.UniqueConstraint("type_id", "user_id"),)

    def __init__(self, type_id, user_id, can_write):
        self.type_id = type_id
        self.user_id = user_id
        self.can_write = can_write


