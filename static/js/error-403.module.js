"use strict";

// Define the `error403` module
angular.module("error403", ["ngRoute"]);

// Register `error403` component, along with its associated controller and template
angular.
module("error403").component("error403", {
    templateUrl: "/static/js/error-403.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function error403Controller($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - Error";
    }]
});