"use strict";

// Define the `eventtypeSharing` module
angular.module("eventtypeSharing", ["ngRoute"]);

// Register `eventtypeSharing` component, along with its associated controller and template
angular.
module("eventtypeSharing").component("eventtypeSharing", {
    templateUrl: "/static/js/eventtype-sharing.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function eventtypeSharingController($http, $routeParams, $location, $rootScope) {
        var self = this;
        var moduleScope = self;
        $rootScope.title = " - Sharing";
        self.is_working = true;
        self.errors = [];
        self.share_users = [];
        self.shares = [];

        self.newshare = {};
        self.newshare.can_write = false;

        $http.get("api/types/" + $routeParams.typeId).then(
            function(response) {
                self.type = response.data;
                self.errors = [];

                //Get the users server URL so we can query for other users for sharing 
                $http.get("api/config").then(
                    function(response) {
                        self.config = response.data;
                        self.errors = [];

                        updateShareTable();
                    },
                    function(error_response) {
                        // Handle errors here
                        self.errors = error_response.data.error_msgs;
                        self.is_working = false;
                    } 
                );
            },
            function(error_response) {
                // Handle errors here
                self.errors = error_response.data.error_msgs;
                self.is_working = false;
            } 
        );

        self.goTypeview = function (){
            $location.path("/types/" + $routeParams.typeId);
        };

        self.shareSubmit = function (){
            self.errors = [];
            self.is_working = true;

            //Options submit all values as strings, and since we want ID to be an integer, we change it here.
            self.newshare["user_id"] = Number(self.newshare["user_id"]);

            $http.post("/api/types/" + $routeParams.typeId + "/shares", self.newshare).then(
                function(response) {
                    self.errors = [];
                    updateShareTable();
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        };

        function updateShareTable(){
            $http.get(self.config.users_host + "/api/users", {withCredentials: true}).then(
                function(response) {
                    self.share_users = response.data;
                    self.errors = [];

                    $http.get("api/types/" + $routeParams.typeId + "/shares").then(
                        function(response) {
                            self.shares = response.data;
                            self.errors = [];
                            self.is_working = false;
                        },
                        function(error_response) {
                            // Handle errors here
                            self.errors = error_response.data.error_msgs;
                            self.is_working = false;
                        } 
                    );
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        }

        self.get_user_info = function(user_id){
            var n = 0;
            for (; n < self.share_users.length; n++){
                if (user_id == self.share_users[n].id){
                    return self.share_users[n].name;
                }
            }
        }

        self.deleteShareSubmit = function (share_id){
            self.errors = [];
            self.is_working = true;
            $http.delete("/api/share/" + share_id).then(
                function(response) {
                    self.errors = [];
                    updateShareTable();
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        };
    }]
});