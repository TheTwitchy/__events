"use strict";

// Define the `main` module
angular.module("main", ["ngRoute"]);

// Register `main` component, along with its associated controller and template
angular.
module("main").component("main", {
    templateUrl: "/static/js/main.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function mainController($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = "";
        self.is_working = true;
        self.errors = [];
        self.newevents = {};
        self.is_event_working = {};
        self.event_errors = {};
        self.event_successes = {};

        $http.get("api/types").then(
            function(response) {
                self.types = response.data;
                self.errors = [];

                var n = 0;
                for (; n < self.types.length; n++){
                    // We need the dictionary set to empty because otherwise it sends an empty POST, with non content-type, which is not allowed for security reasons.
                    self.newevents[self.types[n].id] = {};
                }
                self.is_working = false;
            },
            function(error_response) {
                // Handle errors here
                self.errors = error_response.data.error_msgs;
                self.is_working = false;
            } 
        );

        self.createEventOccurenceSubmit = function (type_id){
            self.event_errors[type_id.toString()] = {};
            self.event_successes[type_id.toString()] = {};

            self.is_event_working[type_id.toString()] = true;

            $http.post("/api/types/" + type_id.toString() + "/events", self.newevents[type_id.toString()]).then(
                function(response) {
                    // Handle success case here
                    self.event_successes[type_id.toString()] = ["Event registered successfully!"];
                    self.is_event_working[type_id.toString()] = false;
                    
                    // Clear the data field on success 
                    document.getElementById("dataInput_" + type_id.toString()).value = "";

                }, 
                function(error_response) {
                    // Handle errors here
                    self.event_errors[type_id.toString()] = error_response.data.error_msgs;
                    self.is_event_working[type_id.toString()] = false;
                }
            );
        };

    }]
});