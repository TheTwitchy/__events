"use strict";

// Define the `eventtypeCreate` module
angular.module("eventtypeCreate", ["ngRoute"]);

// Register `eventtypeCreate` component, along with its associated controller and template
angular.
module("eventtypeCreate").component("eventtypeCreate", {
    templateUrl: "/static/js/eventtype-create.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function eventtypeCreateController($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - New Type";
        self.is_working = false;
        self.errors = [];

        self.neweventtype = {};
        self.neweventtype.has_data = false;

        self.createEventTypeSubmit = function (){
            self.is_working = true;
            $http.post("/api/types", self.neweventtype).then(
                function(response) {
                    // Handle success case here
                    $location.path("/types/" + response.data.id);
                    self.errors = [];
                }, 
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                }
            );
        };

        self.goList = function (){
            $location.path("/");
        };
    }]
});