"use strict";

// Define the `eventtypeDash` module
angular.module("eventtypeDash", ["ngRoute"]);

// Register `eventtypeDash` component, along with its associated controller and template
angular.
module("eventtypeDash").component("eventtypeDash", {
    templateUrl: "/static/js/eventtype-dash.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function eventtypeDashController($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - History";
        self.is_working = true;
        self.errors = [];

        self.chart_data = [];

        $http.get("api/types/" + $routeParams.typeId).then(
            function(response) {
                self.type = response.data;
                self.errors = [];
                eventHistoryUpdate();
            },
            function(error_response) {
                // Handle errors here
                self.errors = error_response.data.error_msgs;
                self.is_working = false;
            } 
        );

        self.goList = function (){
            $location.path("/");
        };

        self.deleteEvent = function(event_id){
            self.is_working = true;

            $http.delete("/api/events/" + event_id.toString()).then(
                function(response) {
                    self.errors = [];
                    eventHistoryUpdate();
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        };

        function eventHistoryUpdate(){
            $http.get("api/types/" + $routeParams.typeId + "/events").then(
                function(response) {
                    self.events = response.data;
                    self.errors = [];
                    self.is_working = false;

                    var style = getComputedStyle(document.body);
                    function unpack(rows, key) {
                        return rows.map(function(row) { return row[key]; });
                    }

                    var dataset = {
                        type: "scatter",
                        mode: "lines+markers",
                        x: unpack(self.events, "time"),
                        y: unpack(self.events, "data"),
                        line: {color: style.getPropertyValue('--info')}
                    };

                    if (!self.type.has_data){
                        dataset.y = []
                        var n = 0;
                        for (;n < self.events.length; n++){
                            dataset.y.push(0);
                        }
                    }

                    var chart_data = [dataset];

                    Plotly.newPlot('eventHistoryChart', chart_data, {}, {responsive: true});

                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        }

        
    }]
});