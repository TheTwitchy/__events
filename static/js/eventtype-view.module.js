"use strict";

// Define the `eventtypeView` module
angular.module("eventtypeView", ["ngRoute"]);

// Register `eventtypeView` component, along with its associated controller and template
angular.
module("eventtypeView").component("eventtypeView", {
    templateUrl: "/static/js/eventtype-view.template.html",
    controller: ["$http", "$routeParams", "$location", "$rootScope", function eventtypeViewController($http, $routeParams, $location, $rootScope) {
        var self = this;
        $rootScope.title = " - Type Settings";
        self.is_working = true;
        self.errors = [];

        $http.get("api/types/" + $routeParams.typeId).then(
            function(response) {
                self.type = response.data;
                self.errors = [];
                self.is_working = false;
            },
            function(error_response) {
                // Handle errors here
                self.errors = error_response.data.error_msgs;
                self.is_working = false;
            } 
        );

        self.editEventTypeSubmit = function (){
            self.errors = [];
            self.is_working = true;
            $http.put("/api/types/" + $routeParams.typeId, self.type).then(
                function(response) {
                    self.errors = [];
                    self.is_working = false;
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        };

        self.deleteTypeSubmit = function (){
            self.errors = [];
            self.is_working = true;
            $http.delete("/api/types/" + $routeParams.typeId).then(
                function(response) {
                    self.errors = [];
                    $location.path("/");
                },
                function(error_response) {
                    // Handle errors here
                    self.errors = error_response.data.error_msgs;
                    self.is_working = false;
                } 
            );
        };

        self.goList = function (){
            $location.path("/");
        };

        self.goSharing = function (){
            $location.path("/types/" + $routeParams.typeId + "/sharing");
        };
    }]
});