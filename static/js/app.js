"use strict";

// Define the `events` module
angular.module("eventsApp", [
    // Dependencies
    "ngRoute",
    "focus-if",
    "main",
    "eventtypeCreate",
    "eventtypeView",
    "eventtypeDash",
    "eventtypeSharing",
    "error403",
    "error404",
    "error500",
]);

angular.
module("eventsApp").
config(["$routeProvider",
    function config($routeProvider) {
        $routeProvider.
        when("/", {
            template: "<main></main>"
        }).
        when("/types/new", {
            template: "<eventtype-create></eventtype-create>"
        }).
        when("/types/:typeId", {
            template: "<eventtype-view></eventtype-view>"
        }).
        when("/types/:typeId/dash", {
            template: "<eventtype-dash></eventtype-dash>"
        }).
        when("/types/:typeId/sharing", {
            template: "<eventtype-sharing></eventtype-sharing>"
        }).
        when("/error/permission", {
            template: "<error-403></error-403>"
        }).
        when("/error/not-found", {
            template: "<error-404></error-404>"
        }).
        when("/error/unknown", {
            template: "<error-500></error-500>"
        }).
        otherwise("/");
    }
]).filter('checkmark', function() {
    return function(input) {
        return input ? '\u2713' : '';
    };
});;

