import os, base64
basedir = os.path.abspath(os.path.dirname(__file__))

def get_env(var):
    try:
        a = os.environ[var]
        return a
    except KeyError:
        return ""

class Development(object):
    FLASK_ENV = "development"
    DEVELOPMENT = True
    DEBUG = True
    CSRF_ENABLED = True
    SECRET_KEY = "sdfknwekjfsenmse43gee5ygenkg4ehuewnd324ye$#erdhSAj"
    
    # From https://elephantsql.com
    SQLALCHEMY_DATABASE_URI = get_env("DATABASE_URL")

    # Service URLs
    # During development, it's a bit hard to test some cross-service stuff due to needing to run several apps at once. Usually not a huge deal in production, but for test, you need a way around it. The way I've done it is that I run one service (normally this one) on 127.0.0.1, and I have a DNS record pointing to that called localhost1.dev.thetwitchy.com. Then, I run the other dev service (usually __users) on 127.0.0.2 (`python manage.py runser --host 127.0.0.2`), which is pointed to by the DNS record localhost2.dev.thetwitchy.com. Then, as long as the root domain for __users is set to dev.thetwitchy.com, I should have about the same setup as I would in prod, and not have to deal with any cross-service shenanigans. This same DNS setup can be replicated on any other domain, I'm just using mine (dev.thetwitchy.com) here.  
    AUTH_SERVICE_URL = "http://localhost2.dev.thetwitchy.com:5000"

    # The b64 encoding here is mostly so that we can just have the ENV be a single line, and not have to deal with multi-line ENV shenanigans
    PUBLIC_KEY = base64.b64decode(get_env("PUBLIC_KEY_B64"))

class Production(Development):
    FLASK_ENV = "production"
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = get_env("SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = get_env("DATABASE_URL")
    AUTH_SERVICE_URL = get_env("AUTH_SERVICE_URL")

    